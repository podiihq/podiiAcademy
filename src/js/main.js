document.getElementById('menu-icon').addEventListener('click', function() {
  document.getElementById('menu').classList.add('show');
  document.getElementById('menu').classList.add('animated');
  document.getElementById('menu').classList.add('slideInDown');
  document.getElementsByTagName('header')[0].classList.add('animated');
  document.getElementsByTagName('header')[0].classList.add('slideInDown');
  document.getElementById('close-button').style.display = 'block';
  document.getElementById('menu-icon').style.display = 'none';
});
document.getElementById('close-button').addEventListener('click', function() {
    document.getElementById('menu').classList.remove('show');
    document.getElementById('menu').classList.remove('animated');
    document.getElementById('menu').classList.remove('slideInDown');
    document.getElementsByTagName('header')[0].classList.remove('animated');
    document.getElementsByTagName('header')[0].classList.remove('slideInDown');
    document.getElementById('close-button').style.display = 'none';
    document.getElementById('menu-icon').style.display = 'block';
});

document.getElementById('prev').addEventListener('click', function() {
  showSlides((slideIndex += -1));
});

document.getElementById('next').addEventListener('click', function() {
  showSlides((slideIndex += 1));
});

var slideIndex = 1;
showSlides(slideIndex);

function currentSlide(n) {
  showSlides((slideIndex = n));
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName('mySlides');
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = 'none';
  }
  slides[slideIndex - 1].style.display = 'block';
}
